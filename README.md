# March_Meeting

LLaVA URL: https://llava.hliu.cc

## Setting up OpenAI API Key

Go to https://platform.openai.com and register for an API Key. You will have to prepay for credits. Once you create the API Key, create a file called `config.json` at the top level of the repo with the following format:

{
"OPEN_AI_KEY": "put-your-key-here"
}

## Downloading Open Source Models

Hermes-2-Pro-Mistral-7B.Q4_K_M.gguf: https://huggingface.co/NousResearch/Hermes-2-Pro-Mistral-7B-GGUF/tree/main

mistral-7b-instruct-v0.2.Q5_K_M.gguf: hhttps://huggingface.co/TheBloke/Mistral-7B-Instruct-v0.2-GGUF/tree/main

## Postgres RAG Setup

Make sure to install Postgres 16 and enable the pgvector plugin to ensure you can store the embeddings.

## LMStudio
Install: lmstudio.ai

## Common Open Source Tooling for Agents/Assistants/Function Calling
Instructor
LangChain
OpenInterpreter

## Langchain Examples
https://github.com/pinecone-io/examples/tree/master/learn/generation/langchain/handbook