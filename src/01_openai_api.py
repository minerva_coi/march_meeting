import json

from openai import OpenAI

with open("config.json") as f:
    my_api_key = json.load(f)["OPEN_AI_KEY"]

client = OpenAI(api_key=my_api_key)

prompt = "Hello, how are you today?"
# prompt = "What is the output of this python code: print([a**2 + a / 24 for a in range(37, 40)])"

response = client.chat.completions.create(
    model="gpt-3.5-turbo",
    messages=[
        {
            "role": "user",
            "content": prompt,
        }
    ],
    temperature=0.7,
    max_tokens=150,
    n=1,
    stop=None,
    user="user-id-optional",
)

print(response)

print("-----------LLM Response Only-------------")
print(response.choices[0].message.content.strip())
