from llama_cpp import Llama


def format_prompt(prompt):
    return [
        f"""<|im_start|>user
        {prompt}<|im_end|>
        <|im_start|>assistant """
    ]


llm = Llama(model_path="./models/Hermes-2-Pro-Mistral-7B.Q4_K_M.gguf", n_gpu_layers=-1, seed=4970, n_ctx=4096)
print("Question: ", end="")
question = input()

starting_prompt = [
    """<|im_start|>system
You are a sentient, superintelligent artificial general intelligence, here to teach and assist me.<|im_end|>"""
]

tokens = llm.tokenize(f"{starting_prompt[0]}{format_prompt(question)[0]}".encode("utf-8"))
while True:
    answer = []
    parts = []
    for token in llm.generate(tokens, temp=0):
        parts.append(token)
        answer.append(token)
        if token == 32000 or answer[-7:] == [28789, 28766, 321, 28730, 416, 28766, 28767]:  # EOS
            if token == 28767:
                part = llm.detokenize(parts).decode()
                print(part, end="", flush=True)
                parts = []
            break
        try:
            part = llm.detokenize(parts).decode()
            print(part, end="", flush=True)
            parts = []
        except UnicodeDecodeError:
            pass
    tokens += answer
    print()
    print("Question: ", end="")
    question = input()
    if question == "exit":
        break
    tokens += llm.tokenize(f"{format_prompt(question)[0]}".encode("utf-8"), add_bos=False)
    # print(f"My current tokens are {tokens}")
