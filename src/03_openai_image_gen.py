import json
from io import BytesIO

import requests
from openai import OpenAI
from PIL import Image

with open("config.json") as f:
    my_api_key = json.load(f)["OPEN_AI_KEY"]

client = OpenAI(api_key=my_api_key)


def generate(user_prompt):
    response = client.images.generate(
        prompt=user_prompt,
        model="dall-e-2",
        n=1,
        response_format="url",
        size="256x256",
    )

    print(response)
    return response.data[0].url


# prompt describing the desired image
text = "batman art in red and blue color"
# calling the custom function "generate"
# saving the output in "url1"
url1 = generate(text)
# using requests library to get the image in bytes
response = requests.get(url1)
# using the Image module from PIL library to view the image
with Image.open(BytesIO(response.content)) as im:
    im.show()
