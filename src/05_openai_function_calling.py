import json

import openai

custom_functions = [
    {
        "name": "extract_student_info",
        "description": "Get the student information from the body of the input text",
        "parameters": {
            "type": "object",
            "properties": {
                "name": {"type": "string", "description": "Name of the person"},
                "major": {"type": "string", "description": "Major subject."},
                "school": {"type": "string", "description": "The university name."},
                "grades": {"type": "integer", "description": "GPA of the student."},
                "club": {"type": "string", "description": "School club for extracurricular activities. "},
            },
        },
    },
    {
        "name": "extract_school_info",
        "description": "Get the school information from the body of the input text",
        "parameters": {
            "type": "object",
            "properties": {
                "name": {"type": "string", "description": "Name of the school."},
                "ranking": {"type": "integer", "description": "QS world ranking of the school."},
                "country": {"type": "string", "description": "Country of the school."},
                "no_of_students": {"type": "integer", "description": "Number of students enrolled in the school."},
            },
        },
    },
]
student_1_description = """David Nguyen is a sophomore majoring in computer science at Stanford University. 
He is Asian American and has a 3.8 GPA. David is known for his programming skills and is an active member of the university's Robotics Club. 
He hopes to pursue a career in artificial intelligence after graduating."""
school_1_description = """Stanford University is a private research university located in Stanford, California, United States.
It was founded in 1885 by Leland Stanford and his wife, Jane Stanford, in memory of their only child, Leland Stanford Jr.
The university is ranked #5 in the world by QS World University Rankings. It has over 17,000 students, including about 7,600 undergraduates and 9,500 graduates"""

description = [student_1_description, school_1_description]

# Initialize clients with API keys
with open("config.json") as f:
    my_api_key = json.load(f)["OPEN_AI_KEY"]

client = openai.OpenAI(api_key=my_api_key)

for i in description:
    response = client.chat.completions.create(
        model="gpt-3.5-turbo",
        messages=[{"role": "user", "content": i}],
        functions=custom_functions,
        function_call="auto",
    )

    # Loading the response as a JSON object
    json_response = json.loads(response.choices[0].message.function_call.arguments)
    print(json_response)


def extract_student_info(name, major, school, grades, club):
    """Get the student information"""

    return f"{name} is majoring in {major} at {school}. He has {grades} GPA and he is an active member of the university's {club}."


def extract_school_info(name, ranking, country, no_of_students):
    """Get the school information"""

    return f"{name} is located in the {country}. The university is ranked #{ranking} in the world with {no_of_students} students."


descriptions = [student_1_description, "Who was a Abraham Lincoln?", school_1_description]

for i, sample in enumerate(descriptions):
    response = client.chat.completions.create(
        model="gpt-3.5-turbo",
        messages=[{"role": "user", "content": sample}],
        functions=custom_functions,
        function_call="auto",
    )

    response_message = response.choices[0].message

    if dict(response_message).get("function_call"):

        # Which function call was invoked
        function_called = response_message.function_call.name

        # Extracting the arguments
        function_args = json.loads(response_message.function_call.arguments)

        # Function names
        available_functions = {"extract_school_info": extract_school_info, "extract_student_info": extract_student_info}

        fuction_to_call = available_functions[function_called]
        response_message = fuction_to_call(*list(function_args.values()))

    else:
        response_message = response_message.content

    print(f"\nSample#{i+1}\n")
    print(response_message)
