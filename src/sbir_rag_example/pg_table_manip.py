import psycopg2


def create_table():
    query = """
            CREATE TABLE sbir_embeddings (
                solicitation_title text,
                solicitation_number text,
                phase text,
                agency text,
                branch text,
                solicitation_year integer,
                close_date date,
                current_status text,
                topic_title text,
                topic_branch text,
                topic_link text,
                topic_chunk_text text,
                embedding_vector vector(384)
            );"""
    return query


def delete_table():
    query = """DROP TABLE IF EXISTS sbir_embeddings;"""
    return query


# Connect to your postgres DB
connection = psycopg2.connect("dbname=sbir-test-db user=jackfrancis")

# Open a cursor to perform database operations
cursor = connection.cursor()
query = "test"
cursor.execute(query)
connection.commit()
cursor.close()
connection.close()
