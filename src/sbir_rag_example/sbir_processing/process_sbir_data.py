import glob
import json
import os
import re
from datetime import datetime

import pandas as pd


def clean_text(text):
    """Clean special characters from the text."""
    text = text.replace("\r\n", " ").replace("\u00a0", " ")
    text = re.sub(r"\s+", " ", text)  # Replace multiple spaces with a single space
    return text.strip()


def get_topic_data(solicitation_info, topic, is_subtopic=False, subtopic_info=None):
    """Helper function to get topic or subtopic data."""
    data = {
        "solicitation_title": clean_text(solicitation_info["solicitation_title"]),
        "solicitation_number": solicitation_info["solicitation_number"],
        "phase": solicitation_info["phase"],
        "agency": solicitation_info["agency"],
        "branch": solicitation_info["branch"],
        "solicitation_year": solicitation_info["solicitation_year"],
        "close_date": solicitation_info["close_date"],
        "current_status": solicitation_info["current_status"],
        "topic_title": clean_text(topic["topic_title"]),
        "topic_branch": topic.get("topic_branch", ""),
        "topic_link": topic.get("sbir_topic_link", ""),
        "topic_description": clean_text(topic.get("topic_description", "")),
    }

    if is_subtopic:
        data["topic_title"] = clean_text(topic["topic_title"] + " " + subtopic_info["subtopic_title"])
        data["topic_branch"] = subtopic_info.get("branch", "")
        data["topic_description"] = clean_text(
            topic["topic_description"] + " " + subtopic_info.get("subtopic_description", "")
        )
        data["topic_link"] = subtopic_info.get("sbir_subtopic_link", "")
    return data


def process_data_chunk(chunk, columns):
    solicitation_info = {key: chunk.get(key, "") for key in columns[:8]}
    all_data = []

    for topic in chunk.get("solicitation_topics", []):
        if len(topic.get("subtopics", [])) == 0:
            all_data.append(get_topic_data(solicitation_info, topic))
        else:
            for subtopic in topic["subtopics"]:
                all_data.append(get_topic_data(solicitation_info, topic, is_subtopic=True, subtopic_info=subtopic))
    return pd.DataFrame(all_data)


def main():
    # Column names for the DataFrame
    columns = [
        "solicitation_title",
        "solicitation_number",
        "phase",
        "agency",
        "branch",
        "solicitation_year",
        "close_date",
        "current_status",
        "topic_title",
        "topic_branch",
        "topic_link",
        "topic_description",
    ]

    df = pd.DataFrame(columns=columns)
    data_files = glob.glob("./data/*")

    for file in data_files:
        with open(file, "r") as f:
            data = json.load(f)
        if not data or not isinstance(data, list):
            print(f"{file} has no data or is not in the expected format.")
            continue

        for solicitation in data:
            if not solicitation.get("solicitation_topics"):
                print(solicitation.get("solicitation_title", "Unknown"), "has no topics")
                continue

            new_df = process_data_chunk(solicitation, columns=columns)
            df = pd.concat([df, new_df], ignore_index=True)

    current_date = datetime.now().strftime("%Y_%m_%d")
    df.to_csv(os.path.join("./processed_data", f"processed_sbirs_{current_date}.csv"))


if __name__ == "__main__":
    main()
