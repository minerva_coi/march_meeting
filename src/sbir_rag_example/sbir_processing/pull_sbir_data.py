import json
import os

import requests


def fetch_and_save_data(agency, status, datatype="json"):
    ctr, modifier = 1, ""
    docs_left = True
    while docs_left:
        docs_left = False
        request_url = (
            f"https://www.sbir.gov/api/solicitations.{datatype}{modifier}?keyword=sbir&agency={agency}&{status}=1"
        )
        try:
            response = requests.get(request_url)
            response.raise_for_status()  # will raise an HTTPError if the HTTP request returned an unsuccessful status code
            data = response.json()
        except requests.RequestException as e:
            print(f"Request failed: {e}")
            break

        file_path = os.path.join("./data", f"{agency}_sbir_{status}_{ctr}.json")
        with open(file_path, "w") as f:
            json.dump(data, f, indent=4)

        if len(data) == 100:  # Assuming 100 is the max number of items per page
            docs_left = True
            ctr += 1
            modifier = f"?start={(ctr - 1) * 100}"


def main():
    status_list = ["closed", "open"]
    agency_list = ["DOD", "NASA", "NSF", "DOT", "DHS"]

    for status in status_list:
        for agency in agency_list:
            fetch_and_save_data(agency, status)
            print(f"Finished with {agency} status {status}")


if __name__ == "__main__":
    main()
