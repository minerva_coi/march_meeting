import psycopg2
from llama_cpp import Llama
from sentence_transformers import SentenceTransformer

connection = psycopg2.connect("dbname=sbir-test-db user=jackfrancis")


def get_rag(question):
    model = SentenceTransformer("all-MiniLM-L6-v2")
    embedded_query = str(model.encode(question).tolist())
    # print(embedded_query.tolist())

    query = """
    SELECT topic_chunk_text
    FROM sbir_embeddings
    ORDER BY embedding_vector <-> %s LIMIT 10;"""

    with connection.cursor() as cursor:
        cursor.execute(query, (embedded_query,))
        text = cursor.fetchall()

    for t in text:
        question += t[0]
    return question


llm = Llama(model_path="./models/mistral-7b-instruct-v0.2.Q5_K_M.gguf", n_gpu_layers=-1, seed=4970, n_ctx=40960)
print("Question: ", end="")
question = input()
# question = get_rag(question)
tokens = llm.tokenize(f"[INST]{question}/INST]".encode("utf-8"))
while True:
    answer = []
    parts = []
    for token in llm.generate(tokens, temp=0):
        parts.append(token)
        answer.append(token)
        if token == 2:  # EOS
            break
        try:
            part = llm.detokenize(parts).decode()
            print(part, end="", flush=True)
            parts = []
        except UnicodeDecodeError:
            pass
    tokens += answer
    print()
    print("Question: ", end="")
    question = input()
    if question == "exit":
        break
    question = get_rag(question)
    tokens += llm.tokenize(f"[INST]{question}[/INST]".encode("utf-8"), add_bos=False)
    print(f"My current tokens are {tokens}")
