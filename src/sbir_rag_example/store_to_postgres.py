import numpy as np
import pandas as pd
import psycopg2
from psycopg2.extensions import AsIs, register_adapter
from sentence_transformers import SentenceTransformer

# Connect to your postgres DB
conn = psycopg2.connect("dbname=sbir-test-db user=jackfrancis")

# Open a cursor to perform database operations
cur = conn.cursor()


# Adapter function to convert numpy arrays to PostgreSQL arrays
def addapt_numpy_float64(numpy_float64):
    return AsIs(numpy_float64)


register_adapter(np.float64, addapt_numpy_float64)

# Load your DataFrame
df = pd.read_csv("./processed_data/processed_sbirs_2024-01-07.csv")  # Modify this with your actual file

# Load the model
model = SentenceTransformer("all-MiniLM-L6-v2")


def chunk_text(text, chunk_size=384, overlap=25):
    words = text.split()
    chunks = [" ".join(words[i : i + chunk_size]) for i in range(0, len(words), chunk_size - overlap)]
    return chunks


# Example function to process and insert data into PostgreSQL using pgvector
def process_and_insert(dataframe):
    for index, row in dataframe.iterrows():
        # Generate embedding
        if isinstance(row["topic_description"], float):
            continue
        text_chunks = chunk_text(row["topic_description"])
        embeddings = model.encode(text_chunks)

        for i, embedding in enumerate(embeddings):
            # Prepare the data for Milvus
            data = (
                row["solicitation_title"],
                row["solicitation_number"],
                row["phase"],
                row["agency"],
                row["branch"],
                row["solicitation_year"],
                row["close_date"],
                row["current_status"],
                row["topic_title"],
                row["topic_branch"],
                row["topic_link"],
                text_chunks[i],
                embedding.tolist() if embedding is not None else None,
            )

            # Insert data into the database
            cur.execute(
                """
                INSERT INTO "sbir_embeddings"
                (solicitation_title, solicitation_number, phase, agency, branch,
                solicitation_year, close_date, current_status, topic_title,
                topic_branch, topic_link, topic_chunk_text, embedding_vector)
                VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
                """,
                data,
            )
        print(index)


# Call the function with your DataFrame
process_and_insert(df)

# Commit changes
conn.commit()

# Close communication with the database
cur.close()
conn.close()
